#  Pizzabot: by gvlcek

## Run the project:
1. Cmd + R
2. Enjoy!

## Run executable:
1. Open the terminal
2. Type Your-Path/PizzaBot/PizzaBot/Pizzabot
3. Enjoy!

## Run tests:
1. Select TestPizzabot scheme
2. Cmd + U
3. Enjoy!

## Build the project:
1. Go to product -> archive
2. After the archive is done select Distribute Content in the organizer
3. Select build products and create a folder for the file
4. Enjoy!
