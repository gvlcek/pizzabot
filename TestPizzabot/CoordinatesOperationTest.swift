//
//  CoordinatesOperationTest.swift
//  TestPizzabot
//
//  Created by Guadalupe Vlcek on 26/09/2021.
//

import XCTest

@testable import Pizzabot

class CoordinatesOperationTest: XCTestCase {
    var coordinatesOperation: CoordinatesOperation!
    let coordinates = [Coordinates(latitude: 2, longitude: 1),
                       Coordinates(latitude: 4, longitude: 4),
                       Coordinates(latitude: 1, longitude: 3)]
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        coordinatesOperation = CoordinatesOperation(matrix: Coordinates(latitude: 5, longitude: 5), coordinates: coordinates)
    }

    override func tearDownWithError() throws {
        coordinatesOperation = nil
    }
    
    func testValidate() throws {
        let matrix = Coordinates(latitude: 5, longitude: 5)
        let coordinates = [Coordinates(latitude: 0, longitude: 0),
                           Coordinates(latitude: 1, longitude: 3),
                           Coordinates(latitude: 6, longitude: 4)]
        let coordOperation = CoordinatesOperation(matrix: matrix, coordinates: coordinates).coordinates
        XCTAssertEqual(coordOperation, [Coordinates(latitude: 0, longitude: 0), Coordinates(latitude: 1, longitude: 3)])
    }
    
    func testOptimization() throws {
        let optimization = coordinatesOperation.optimizeCoordinatesList()
        XCTAssertEqual(optimization, [Coordinates(latitude: 4, longitude: 4),
                                      Coordinates(latitude: 1, longitude: 3),
                                      Coordinates(latitude: 2, longitude: 1)])
    }
}
