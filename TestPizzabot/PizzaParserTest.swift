//
//  PizzaParserTest.swift
//  TestPizzabot
//
//  Created by Guadalupe Vlcek on 26/09/2021.
//

import XCTest

@testable import Pizzabot

class PizzaParserTest: XCTestCase {
    var pizzaParser: PizzaParser!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        pizzaParser = PizzaParser()
    }

    override func tearDownWithError() throws {
        pizzaParser = nil
    }
    
    func testRegex() throws {
        XCTAssertTrue(pizzaParser.regexValidator(input: "5x5 (0, 0) (1, 3) (4,4) (4, 2) (4, 2) (0, 1) (3, 2) (2, 3) (4, 1)"))
    }
    
    func testMatrix() throws {
        XCTAssertEqual(pizzaParser.parseMatrixFromString(input: "5x5 (0, 0) (1, 3) (4,4) (4, 2) (4, 2) (0, 1) (3, 2) (2, 3) (4, 1)"), Coordinates(latitude: 5, longitude: 5))
    }
    
    func testCoordinates() throws {
        let coordinates = [Coordinates(latitude: 0, longitude: 0),
                           Coordinates(latitude: 1, longitude: 3),
                           Coordinates(latitude: 4, longitude: 4),
                           Coordinates(latitude: 4, longitude: 2),
                           Coordinates(latitude: 4, longitude: 2),
                           Coordinates(latitude: 0, longitude: 1),
                           Coordinates(latitude: 3, longitude: 2),
                           Coordinates(latitude: 2, longitude: 3),
                           Coordinates(latitude: 4, longitude: 1)]
        
        XCTAssertEqual(pizzaParser.parseCoordinatesFromString(input: "5x5 (0, 0) (1, 3) (4,4) (4, 2) (4, 2) (0, 1) (3, 2) (2, 3) (4, 1)"), coordinates)
    }
}
