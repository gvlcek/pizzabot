//
//  TestMathOperation.swift
//  TestPizzabot
//
//  Created by Guadalupe Vlcek on 26/09/2021.
//

import XCTest

@testable import Pizzabot

class TestMathOperation: XCTestCase {
    var mathOperation: MathOperation!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        mathOperation = MathOperation()
    }

    override func tearDownWithError() throws {
        mathOperation = nil
    }
    
    func testCalculateDistance() throws {
        XCTAssertEqual(MathOperation.calculateDistanceBetweenTwoPoints(x2: 0, y2: 2), 2)
        XCTAssertEqual(MathOperation.calculateDistanceBetweenTwoPoints(x1: 2, x2: 2, y1: 6, y2: 4), 2)
    }
}
