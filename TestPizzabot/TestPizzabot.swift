//
//  TestPizzabot.swift
//  TestPizzabot
//
//  Created by Guadalupe Vlcek on 21/09/2021.
//

import XCTest

@testable import Pizzabot

class TestPizzabot: XCTestCase {
    var pizzabot: Pizzabot!

    override func setUpWithError() throws {
        try super.setUpWithError()
        pizzabot = Pizzabot()
    }

    override func tearDownWithError() throws {
        pizzabot = nil
    }
    
    // MARK: - Main test
    
    func testPizzabot() throws {
        //5x5 (0, 0) (1, 3) (4,4) (4, 2) (4, 2) (0, 1) (3, 2) (2, 3) (4, 1)
        
        let matrix = Coordinates(latitude: 5, longitude: 5)
        let coordinates = [Coordinates(latitude: 0, longitude: 0),
                           Coordinates(latitude: 1, longitude: 3),
                           Coordinates(latitude: 4, longitude: 4),
                           Coordinates(latitude: 4, longitude: 2),
                           Coordinates(latitude: 4, longitude: 2),
                           Coordinates(latitude: 0, longitude: 1),
                           Coordinates(latitude: 3, longitude: 2),
                           Coordinates(latitude: 2, longitude: 3),
                           Coordinates(latitude: 4, longitude: 1)
        ]
        XCTAssertEqual(pizzabot.getInstructions(matrixDimensions: matrix, addressCoordinates: coordinates),"EEEENNNNDSSDDWDWNDEESSDWWWNNDWSSDSD")
    }
    
    // MARK: Latitude Calculation
    
    func testLatitudeInstructions() throws {
        XCTAssertEqual(pizzabot.addLatitudeInstructions(lastLatitude: 0, currentlatitude: 4), "EEEE")
        XCTAssertEqual(pizzabot.addLatitudeInstructions(lastLatitude: 3, currentlatitude: 0), "WWW")
        XCTAssertNil(pizzabot.addLatitudeInstructions(lastLatitude: 1, currentlatitude: 1))
    }
    
    // MARK: Longitude Calculation
    
    func testLongitudeInstructions() throws {
        XCTAssertEqual(pizzabot.addLongitudeInstructions(lastLongitude: 0, currentLongitude: 5), "NNNNN")
        XCTAssertEqual(pizzabot.addLongitudeInstructions(lastLongitude: 3, currentLongitude: 1), "SS")
        XCTAssertNil(pizzabot.addLongitudeInstructions(lastLongitude: 1, currentLongitude: 1))
    }
}
