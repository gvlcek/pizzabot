//
//  PizzaParser.swift
//  Pizzabot
//
//  Created by Guadalupe Vlcek on 26/09/2021.
//

import Foundation

struct PizzaParser {
    func performParsing(input: String) throws -> (matrix: Coordinates, coordinates: [Coordinates]) {
        if regexValidator(input: input) {
            guard let matrix = parseMatrixFromString(input: input), let coordinates = parseCoordinatesFromString(input: input) else {
                throw PizzabotError.matrixParsingError
            }
            return (matrix, coordinates)
        } else {
            throw PizzabotError.parsingError
        }
    }
    
    /// Using regex validates the stream input format
    
    func regexValidator(input: String) -> Bool {
        let inputPattern = #"^(\dx\d ?)(\(\d, ?\d\) ?)+"#
        return input.range(of: inputPattern, options: .regularExpression) != nil ? true : false
    }
    
    /// Using regex parses the string and gets the matrix
    
    func parseMatrixFromString(input: String) -> Coordinates? {
        let matrixPattern = #"^(\dx\d ?)"#
        guard let matrixRange = input.range(of: matrixPattern, options: .regularExpression) else { return nil }
        let matrixString = input[matrixRange]
        let matrixCoordinates = matrixString.components(separatedBy: "x")
        guard let latitude = Int(matrixCoordinates.first?.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted) ?? "") else {
            print(PizzabotError.matrixParsingError.rawValue)
            return nil
        }
        guard let longitude = Int(matrixCoordinates.last?.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted) ?? "") else {
            print(PizzabotError.matrixParsingError.rawValue)
            return nil
        }
        return Coordinates(latitude: latitude, longitude: longitude)
    }
    
    /// Using regex parses the string and gets the coordinates
    
    func parseCoordinatesFromString(input: String) -> [Coordinates]? {
        let coordinatesPattern = #"(\(\d, ?\d\) ?)+"#
        let singleCoordinatePattern = #"^(\(\d, ?\d\) ?)"#
        
        var coordinateValue: [String] = []
        if let coordinatesRange = input.range(of: coordinatesPattern, options: .regularExpression) {
            var coordinatesString = input[coordinatesRange]
            while coordinatesString != "" {
                if let coordinateRange = coordinatesString.range(of: singleCoordinatePattern, options: .regularExpression) {
                    coordinateValue.append(String(coordinatesString[coordinateRange]).filter("0123456789,".contains))
                    coordinatesString.removeSubrange(coordinateRange)
                }
            }
        }
        
        var coordinates: [Coordinates] = []
        coordinateValue.forEach({
            let components = $0.components(separatedBy: ",")
            guard let latitude = Int(components.first ?? "") else {
                print(PizzabotError.coordinatesParsingError.rawValue)
                return
            }
            guard let longitude = Int(components.last ?? "") else {
                print(PizzabotError.coordinatesParsingError.rawValue)
                return
            }
            coordinates.append(Coordinates(latitude: latitude, longitude: longitude))
        })
        
        return coordinates
    }
}
