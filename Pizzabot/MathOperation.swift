//
//  MathOperation.swift
//  Pizzabot
//
//  Created by Guadalupe Vlcek on 25/09/2021.
//

import Foundation

struct MathOperation {
    /// Distance between two points
    /// 
    /// Given two coordinates A(x1, y1) B(x2, y2) it applies the formula:
    ///
    /// √((x2 - x1)² + (y2 - y1)²)
    ///
    /// and returns the distance between the two points
    static func calculateDistanceBetweenTwoPoints(x1: Int = 0, x2: Int, y1: Int = 0, y2: Int) -> Double {
        let powValues = pow(Decimal((x2 - x1)), 2) + pow(Decimal((y2 - y1)), 2)
        return sqrt(Double(truncating: powValues as NSNumber))
    }
}
