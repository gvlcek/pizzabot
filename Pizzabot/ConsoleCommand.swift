//
//  ConsoleCommand.swift
//  Pizzabot
//
//  Created by Guadalupe Vlcek on 20/09/2021.
//

import Foundation

enum PizzabotError: String, Error {
    case invalidInput = "Invalid format input"
    case streamError = "Keyboard error"
    case parsingError = "Error parsing the input"
    case matrixParsingError = "Invalid Matrix"
    case coordinatesParsingError = "Invalid Coordinates"
}

struct ConsoleCommand {
    static func getInput() throws -> String {
        let keyboard = FileHandle.standardInput
        let inputData = keyboard.availableData
        guard let strData = String(data: inputData, encoding: String.Encoding.utf8) else {
            throw PizzabotError.invalidInput
        }
        return strData.trimmingCharacters(in: CharacterSet.newlines)
    }
}
