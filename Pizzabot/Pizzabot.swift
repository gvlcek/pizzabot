//
//  Pizzabot.swift
//  Pizzabot
//
//  Created by Guadalupe Vlcek on 21/09/2021.
//

import Foundation

struct Pizzabot {
    private let originCoordinates = Coordinates(latitude: 0, longitude: 0)
    
    func getInstructions(matrixDimensions: Coordinates, addressCoordinates: [Coordinates]) -> String {
        let coordinates = CoordinatesOperation(matrix: matrixDimensions, coordinates: addressCoordinates).optimizeCoordinatesList()
        
        var lastCoordinates = originCoordinates
        var instructions: String = ""

        coordinates.forEach({
            let currentCoordinates = $0
            instructions += addLatitudeInstructions(lastLatitude: lastCoordinates.latitude, currentlatitude: currentCoordinates.latitude) ?? ""
            instructions += addLongitudeInstructions(lastLongitude: lastCoordinates.longitude, currentLongitude: currentCoordinates.longitude) ?? ""
            instructions += addDropInstruction()
            lastCoordinates = currentCoordinates
        })

        return instructions
    }

    // MARK: - Function Instructions

    func addLatitudeInstructions(lastLatitude: Int, currentlatitude: Int) -> String? {
        var instructionString = ""
        if currentlatitude > lastLatitude {
            for _ in 0..<lastLatitude.distance(to: currentlatitude) {
                instructionString += Instruction.east.rawValue
            }
        } else if currentlatitude < lastLatitude {
            for _ in 0..<currentlatitude.distance(to: lastLatitude) {
                instructionString += Instruction.west.rawValue
            }
        } else {
            return nil
        }
        return instructionString
    }

    func addLongitudeInstructions(lastLongitude: Int, currentLongitude: Int) -> String? {
        var instructionString = ""
        if currentLongitude > lastLongitude {
            for _ in 0..<lastLongitude.distance(to: currentLongitude) {
                instructionString += Instruction.north.rawValue
            }
        } else if currentLongitude < lastLongitude {
            for _ in 0..<currentLongitude.distance(to: lastLongitude) {
                instructionString += Instruction.south.rawValue
            }
        } else {
            return nil
        }
        return instructionString
    }

    func addDropInstruction() -> String {
        return Instruction.drop.rawValue
    }
}
