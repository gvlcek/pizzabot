//
//  main.swift
//  PizzaBot
//
//  Created by Guadalupe Vlcek on 18/09/2021.
//

import Foundation

let pizzabot = Pizzabot()
let pizzaParser = PizzaParser()

// MARK: - Main Flow

print("Welcome to Pizza bot!\nPlease Insert the dimensions of the grid and the coordinates.\n")
do {
    let stringInput = try ConsoleCommand.getInput()
    let parsedData = try pizzaParser.performParsing(input: stringInput)
    let matrix = parsedData.matrix
    let coordinates = parsedData.coordinates
    print(pizzabot.getInstructions(matrixDimensions: matrix, addressCoordinates: coordinates))
} catch PizzabotError.streamError {
    print(PizzabotError.streamError.rawValue)
} catch PizzabotError.matrixParsingError {
    print(PizzabotError.matrixParsingError.rawValue)
} catch PizzabotError.parsingError {
    print(PizzabotError.parsingError.rawValue)
}
