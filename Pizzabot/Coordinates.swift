//
//  Coordinates.swift
//  PizzaBot
//
//  Created by Guadalupe Vlcek on 18/09/2021.
//

import Foundation

struct Coordinates: Equatable {
    var latitude: Int
    var longitude: Int
    
    init(latitude: Int, longitude: Int) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

struct CoordinateWithDistance {
    var distanceFromOrigin: Double!
    var coordinates: Coordinates!
}

struct CoordinatesOperation {
    private let matrix: Coordinates!
    var coordinates: [Coordinates] = []
    
    init(matrix: Coordinates, coordinates: [Coordinates]) {
        self.matrix = matrix
        self.coordinates = validateCoordinates(coordinates: coordinates)
    }
    
    /// Validates the coordinates againts the matrix
    
    func validateCoordinates(coordinates: [Coordinates]) -> [Coordinates] {
        var coordinatesValidated: [Coordinates] = []
        coordinates.forEach({
            var value = $0
            while (value.latitude > matrix.latitude || value.longitude > matrix.longitude) {
                print("Invalid input: (\($0.latitude), \($0.longitude))\nPlease write a valid value for \(matrix.latitude)x\(matrix.longitude)")
                let stringInput = try? ConsoleCommand.getInput()
                let pizzaParser = PizzaParser()
                if let newValue = pizzaParser.parseCoordinatesFromString(input: stringInput ?? "")?.first {
                    value = newValue
                }
            }
            coordinatesValidated.append(value)
        })
        return coordinatesValidated
    }
    
    /// Returns an array of the coordinates sorted with an optimized route
    
    func optimizeCoordinatesList() -> [Coordinates] {
        var currentCoordinates = coordinates
        
        //If there are only two coordinates is not necessary to optimize the route
        if currentCoordinates.count <= 2 { return currentCoordinates }
        
        //Gets the distances from the point (0, 0)
        var origin = Coordinates(latitude: 0, longitude: 0)
        let sortedCoordinates = distanceFromOrigin(origin: origin, values: currentCoordinates)
        
        //Gets the coordinate furthest from (0, 0) and it assigns it makes it the new origin
        guard let distanceFromZero = sortedCoordinates.sorted(by: { $0.distanceFromOrigin > $1.distanceFromOrigin }).first else { return currentCoordinates }
        origin = distanceFromZero.coordinates
        
        //Removes the new origin from the list of coordinates
        currentCoordinates.removeAll(where: { $0 == origin })
        
        //Get the distances from the new origin
        let furthestCoordinate = distanceFromOrigin(origin: origin, values: currentCoordinates)
        //Sort the distances form the closest to the furthest
        let distanceFromFurthest = furthestCoordinate.sorted(by: { $0.distanceFromOrigin < $1.distanceFromOrigin })
        
        //The first coordinate is the new origin
        var optimizedCoordinates = [origin]
        
        //Adds the sorted coordinates
        distanceFromFurthest.forEach({ optimizedCoordinates.append($0.coordinates) })
        return optimizedCoordinates
    }

    /// Given an origin Coordinate and an array Coordinate returns an array of the coordinates with the distance from the origin

    private func distanceFromOrigin(origin: Coordinates, values: [Coordinates]) -> [CoordinateWithDistance] {
        var coordinateWithDistance: [CoordinateWithDistance] = []
        values.forEach({
            coordinateWithDistance.append(CoordinateWithDistance(distanceFromOrigin: MathOperation.calculateDistanceBetweenTwoPoints(x1: origin.longitude, x2: $0.longitude, y1: origin.latitude, y2: $0.latitude), coordinates: $0))
        })
        return coordinateWithDistance
    }
}

