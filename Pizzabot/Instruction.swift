//
//  Instruction.swift
//  PizzaBot
//
//  Created by Guadalupe Vlcek on 18/09/2021.
//

import Foundation

enum Instruction: String {
    case north = "N"
    case south = "S"
    case east = "E"
    case west = "W"
    case drop = "D"
}
